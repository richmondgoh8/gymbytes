local composer = require ("composer")		

display.setStatusBar( display.DefaultStatusBar )
display.setDefault( "background", 0, 0, 0 )

composer.setVariable("universalFont","assets/fonts/Roboto-Medium.ttf")
composer.setVariable("variableString", "Game Level")
composer.setVariable("universalBG","assets/ui/ultimate.jpg")
composer.setVariable("fontSize", 64)
composer.setVariable("speed",0)
composer.setVariable("statusHeight",display.topStatusBarContentHeight)

composer.gotoScene("_screens.init", "crossFade", 0)