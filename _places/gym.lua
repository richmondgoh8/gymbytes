local gym = {
  {
    title = "The Little Gym of Singapore East",
    lat = 1.2920428,
    lng = 103.8565473,
  },
  {
    title = "Genesis Gym",
    lat = 1.2844376,
    lng = 103.8499141,
  },
  {
    title = "Pure Fitness Ocean Financial Centre",
    lat = 1.2832329,
    lng = 103.8520207,
  },
  {
    title = "Fitness First Singapore Pte Ltd",
    lat = 1.277781,
    lng = 103.84754,
  },
  {
    title = "Personal Trainer Singapore -Genesis Gym- Smart Fitness,Total Health - Paya Lebar",
    lat = 1.319032,
    lng = 103.897447,
  },
  {
    title = "Gymmboxx",
    lat = 1.3251487,
    lng = 103.9322635,
  },
  {
    title = "The Grit Gym",
    lat = 1.2879119,
    lng = 103.8478544,
  },
  {
    title = "TripleFit Singapore",
    lat = 1.293614,
    lng = 103.859664,
  },
  {
    title = "True Fitness Chevron House",
    lat = 1.2842253,
    lng = 103.8518212,
  },
  {
    title = "My Gym @ Ang Mo Kio",
    lat = 1.372882,
    lng = 103.847666,
  },
  {
    title = "The Little Gym",
    lat = 1.306267,
    lng = 103.8286456,
  },
  {
    title = "My Gym @ Tampines",
    lat = 1.3527155,
    lng = 103.9424966,
  },
  {
    title = "Anytime Fitness",
    lat = 1.378398,
    lng = 103.941887,
  },
  {
    title = "My Gym",
    lat = 1.3114973,
    lng = 103.8566861,
  },
  {
    title = "My Gym @ Jurong East",
    lat = 1.3343916,
    lng = 103.7428247,
  },
  {
    title = "My Gym @ Marine Parade",
    lat = 1.3052111,
    lng = 103.9050598,
  },
  {
    title = "Anytime Fitness Tampines Mart",
    lat = 1.3541674,
    lng = 103.9603667,
  },
  {
    title = "Personal Trainer Singapore - Genesis Gym: Smart Fitness,Total Health - Alexandra",
    lat = 1.291365,
    lng = 103.807561,
  },
  {
    title = "Anytime Fitness NEX",
    lat = 1.3510305,
    lng = 103.8724054,
  },
  {
    title = "Anytime Fitness Greenwich",
    lat = 1.3876872,
    lng = 103.8695395,
  },
  {
    title = "My Gym @ Marine Parade",
    lat = 1.3052111,
    lng = 103.9050598,
  },
  {
    title = "Gold's Gym Pte Ltd - Bukit Merah",
    lat = 1.282175,
    lng = 103.817879,
  },

 {
    title = "Gold's Gym Personal Training - Devonshire",
    lat = 1.296361,
    lng = 103.837247,
  },

 {
    title = "Anytime Fitness Bedok North",
    lat = 1.3264419,
    lng = 103.9299937,
  },
  {
    title = "Delta ActiveSG Gym",
    lat = 1.2904764,
    lng = 103.8205176,
  },
  {
    title = "SAFRA EnergyOne",
    lat = 1.344724,
    lng = 103.941739,
  },
  {
    title = "EnergyOne Gym (SAFRA Punggol)",
    lat = 1.4104821,
    lng = 103.9059519,
  },
  {
    title = "Intrepid Kids Gym",
    lat = 1.303788,
    lng = 103.902955,
  },
  {
    title = "SAFRA EnergyOne",
    lat = 1.277742,
    lng = 103.816971,
  },
  {
    title = "SAFRA EnergyOne Jurong",
    lat = 1.335166,
    lng = 103.706299,
  },
  {
    title = "Virgin Active Raffles Place",
    lat = 1.2846881,
    lng = 103.8511575,
  },
  {
    title = "Contours Express Women's Gym",
    lat = 1.359627,
    lng = 103.8420735,
  },
  {
    title = "Dennis Gym",
    lat = 1.3451008,
    lng = 103.9551687,
  },
  {
    title = "The Strength Yard",
    lat = 1.313442,
    lng = 103.9016806,
  },
  {
    title = "Muse Fitness Club",
    lat = 1.311214,
    lng = 103.859116,
  },
  {
    title = "Fitness First",
    lat = 1.278903,
    lng = 103.854483,
  },
  {
    title = "Fitness First Westgate",
    lat = 1.3343362,
    lng = 103.7432853,
  },
  {
    title = "Hougang ActiveSG Sports Centre",
    lat = 1.3707785,
    lng = 103.8881584,
  },
  {
    title = "Energia Fitness",
    lat = 1.2892694,
    lng = 103.8442408,
  },
  {
    title = "Anytime Fitness Queensway",
    lat = 1.287834,
    lng = 103.803611,
  },
  {
  title = "True Fitness (Suntec City Mall)",
  lat = 1.2963108,
  lng = 103.8586338,
 },
 {
  title = "Toa Payoh ActiveSG Gym",
  lat = 1.3305678,
  lng = 103.8506526,
 },
 {
  title = "Virgin Active Tanjong Pagar",
  lat = 1.276859,
  lng = 103.845786,
 },
 {
  title = "Anytime Fitness Paya Lebar",
  lat = 1.3143314,
  lng = 103.894432,
 },
 {
  title = "Bukit Gombak ActiveSG Gym",
  lat = 1.3596207,
  lng = 103.7523688,
 },
 {
  title = "Gold's Gym Personal Training - Purvis",
  lat = 1.2966794,
  lng = 103.8549952,
 },
 {
  title = "Anytime Fitness Jurong Central",
  lat = 1.350415,
  lng = 103.729003,
 },
 {
  title = "Gold's Gym Personal Training - Farrer Park",
  lat = 1.31652,
  lng = 103.85713,
 },
 {
  title = "Anytime Fitness Buangkok",
  lat = 1.3792941,
  lng = 103.8878182,
 },
 {
  title = "Ritual Gym (Holland Village)",
  lat = 1.310352,
  lng = 103.795081,
 },
 {
  title = "Fitness First - Paragon",
  lat = 1.30368,
  lng = 103.835468,
 },
 {
  title = "Contours Express Women's Gym",
  lat = 1.4269244,
  lng = 103.8373291,
 },
 {
  title = "Gold's Gym Personal Training - Buona Vista (One North, NTU Alumni Club)",
  lat = 1.303709,
  lng = 103.789637,
 },
 {
  title = "JWT Kids Gym",
  lat = 1.2921264,
  lng = 103.8426986,
 },
 {
  title = "Parkour Gym Singapore - Free Runner Lodge - A2 Parkour",
  lat = 1.3116418,
  lng = 103.8632944,
 },
 {
  title = "True Fitness",
  lat = 1.263602,
  lng = 103.820157,
 },
 {
  title = "Anytime Fitness Tanjong Pagar",
  lat = 1.2760728,
  lng = 103.8459096,
 },
 {
  title = "Bishan ActiveSG Gym",
  lat = 1.3555077,
  lng = 103.8510132,
 },
 {
  title = "Anytime Fitness Upper Cross Street",
  lat = 1.28443,
  lng = 103.84622,
 },
 {
  title = "Anytime Fitness Kallang Wave",
  lat = 1.3031664,
  lng = 103.8728465,
 },

}

return gym
