local clinic = {
  {
    title = "DSC Clinic",
    lat = 1.3065413,
    lng = 103.8570136,
  },
  {
    title = "Novena Medical Center Singapore",
    lat = 1.3206356,
    lng = 103.844392,
  },
  {
    title = "International Medical Clinic - Camden",
    lat = 1.3031958,
    lng = 103.8238267,
  },
  {
    title = "Central 24-HR Clinic (Jurong West)",
    lat = 1.3498774,
    lng = 103.7247446,
  },
  {
    title = "Singapore Buddhist Free Clinic (Redhill Branch)",
    lat = 1.292486,
    lng = 103.8152349,
  },
  {
    title = "Central 24-HR Clinic (Clementi)",
    lat = 1.3135086,
    lng = 103.7654817,
  },
  {
    title = "Central 24-HR Clinic (Woodlands)",
    lat = 1.4456342,
    lng = 103.7981623,
  },
  {
    title = "Travellers' Health & Vaccination Clinic (THVC)",
    lat = 1.321097,
    lng = 103.84564,
  },
  {
    title = "Central Clinic 24 Hrs",
    lat = 1.3725113,
    lng = 103.8857407,
  },
  {
    title = "Tanjong Pagar Medical Clinic",
    lat = 1.2748537,
    lng = 103.8426871,
  },
  {
    title = "International Medical Clinic - Jelita",
    lat = 1.317778,
    lng = 103.786058,
  },
  {
    title = "Healthway Medical Clinic",
    lat = 1.402247,
    lng = 103.901371,
  },
  {
    title = "Healthway Medical Clinic",
    lat = 1.37274,
    lng = 103.94915,
  },
  {
    title = "Singapore Buddhist Free Clinic (Sembawang)",
    lat = 1.4505216,
    lng = 103.8215858,
  },
  {
    title = "Kensington Family Clinic",
    lat = 1.3653191,
    lng = 103.8661955,
  },
  {
    title = "Women's Clinic of Singapore",
    lat = 1.3723928,
    lng = 103.8479836,
  },
  {
    title = "Cutis Medical Laser Clinics",
    lat = 1.306649,
    lng = 103.832056,
  },
  {
    title = "Singapore Women's Clinic",
    lat = 1.348648,
    lng = 103.934491,
  },
  {
    title = "Lian Clinic",
    lat = 1.443392,
    lng = 103.7778664,
  },
  {
    title = "Pinnacle Family Clinic - River Valley",
    lat = 1.2941023,
    lng = 103.8423096,
  },
  {
  title = "Silver Cross Family Clinic",
  lat = 1.3504398,
  lng = 103.7181922,
 },
 {
  title = "Killiney Family & Wellness Clinic Pte Ltd",
  lat = 1.2989327,
  lng = 103.8395406,
 },
 {
  title = "Privé",
  lat = 1.3065772,
  lng = 103.8294684,
 },
 {
  title = "Australia Clinic",
  lat = 1.352852,
  lng = 103.836423,
 },
 {
  title = "The Travel Clinic",
  lat = 1.2814654,
  lng = 103.8367412,
 },
 {
  title = "Healthway Medical Clinic",
  lat = 1.4305233,
  lng = 103.8280335,
 },
 {
  title = "Marsiling Clinic & Surgery",
  lat = 1.4384989,
  lng = 103.7789724,
 },
 {
  title = "Mind Care Clinic (Psychiatrist, Singapore)",
  lat = 1.312181,
  lng = 103.854146,
 },
 {
  title = "Nobel Psychological Wellness Clinic",
  lat = 1.368902,
  lng = 103.85612,
 },
 {
  title = "The Chelsea Clinic",
  lat = 1.3047792,
  lng = 103.8310174,
 },
 {
  title = "The Chelsea Clinic",
  lat = 1.2647924,
  lng = 103.8221695,
 },
 {
  title = "W Eye Clinic",
  lat = 1.2941181,
  lng = 103.8578846,
 },
 {
  title = "Tanglin Medical Clinic",
  lat = 1.306364,
  lng = 103.827018,
 },
 {
  title = "The Chelsea Clinic",
  lat = 1.3055306,
  lng = 103.788355,
 },
 {
  title = "SL Clinic",
  lat = 1.3007,
  lng = 103.845062,
 },
 {
  title = "UFIT Clinic",
  lat = 1.2817911,
  lng = 103.8514035,
 },
 {
  title = "SBCC Baby & Child Clinic (Bishan)",
  lat = 1.3576216,
  lng = 103.844134,
 },
 {
  title = "Unihealth 24-Hr Clinic",
  lat = 1.3334947,
  lng = 103.8492882,
 },
 {
  title = "Ong's Clinic",
  lat = 1.302126,
  lng = 103.852736,
 },
 {
  title = "Tham Clinic Singapore Pte.Ltd.",
  lat = 1.3224839,
  lng = 103.8530034,
 },
 {
  title = "Eye Clinic Singapore International",
  lat = 1.3499084,
  lng = 103.7266598,
 },
 {
  title = "Healthway Medical Clinic",
  lat = 1.3414056,
  lng = 103.6914451,
 },
 {
  title = "Eye Clinic Singapore International",
  lat = 1.3722782,
  lng = 103.8480043,
 },
 {
  title = "Resilienz Clinic (Singapore Psychiatrist and Psychologist)",
  lat = 1.320409,
  lng = 103.844151,
 },
 {
  title = "Singapore Thong Chai Medical Institution - Ang Mo Kio Community Clinic",
  lat = 1.3690109,
  lng = 103.8515655,
 },
 {
  title = "Healthway International Medical Clinic",
  lat = 1.3005142,
  lng = 103.8376372,
 },
 {
  title = "Banyan Clinic",
  lat = 1.4460814,
  lng = 103.7980428,
 },
 {
  title = "Women's Health Clinic Singapore",
  lat = 1.3018829,
  lng = 103.853117,
 },
 {
  title = "The Endocrine Clinic",
  lat = 1.3220241,
  lng = 103.8443011,
 },
 {
  title = "Faith Medical Clinic & Surgery",
  lat = 1.360636,
  lng = 103.9574279,
 },
 {
  title = "Clariancy Aesthetic Clinic Singapore",
  lat = 1.295764,
  lng = 103.8542587,
 },
 {
  title = "Soma Clinic",
  lat = 1.3055135,
  lng = 103.8300106,
 },
 {
  title = "Healthway Woodlands Family Clinic",
  lat = 1.4381681,
  lng = 103.7950804,
 },
 {
  title = "Singapore Bone and Joint Clinic",
  lat = 1.3075759,
  lng = 103.9069238,
 },
 {
  title = "Anonymous HIV Testing Clinic (Dr Tan and Partners @Robertson)",
  lat = 1.292061,
  lng = 103.8417982,
 },
 {
  title = "Hong Family Clinic",
  lat = 1.3476925,
  lng = 103.9524785,
 },
 {
  title = "The Sole Clinic @ Tanglin",
  lat = 1.306364,
  lng = 103.826975,
 },
 {
  title = "Dr Tyng Tan Aesthetics and Hair Clinic | Hair Transplant Singapore",
  lat = 1.3027623,
  lng = 103.8342901,
 },
 {
  title = "SBCC Baby & Child Clinic (Neurology Centre)",
  lat = 1.303536,
  lng = 103.835688,
 },

}

return clinic
