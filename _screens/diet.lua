local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local sqlite3 = require( "sqlite3" )
local scene = composer.newScene()

local goalTitle,path,db

local function getGoals()
    local userInfo = loadsave.loadTable("personalData.json")
    goalTitle.text = "("..userInfo.goalName..")"
end

local function onRowRender( event )

    --Set up the localized variables to be passed via the event table

    local row = event.row
    local id = row.index
    local params = event.row.params

    row.bg = display.newRect( 0, 0, display.contentWidth, 60 )
    row.bg.anchorX = 0
    row.bg.anchorY = 0
    row.bg:setFillColor( 1, 1, 1 )
    row:insert( row.bg )

    local options =
        {
            text = params.name,
            width = display.contentWidth-65,
            font = native.systemFont,
            fontSize = 18,
            align = "center"  -- Alignment parameter
        }

    row.nameText = display.newText( options )

    --row.nameText = display.newText( params.name, row.width, 0,128, native.systemFontBold, 18,"" )
    row.nameText.anchorX = 0
    row.nameText.anchorY = 0.5
    row.nameText:setFillColor( 0 )
    row.nameText.y = 20
    row.nameText.x = -15

    row:insert( row.nameText )
    --row:insert( row.phoneText )

    return true
end

function scene:create( event )

    local sceneGroup = self.view

    local customFont = composer.getVariable("universalFont")
    local defaultBG = composer.getVariable("universalBG")

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight


    local menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = centerX
    menubg.y = centerY + statusbarHeight

    local cardBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth,fullHeight - 50 )
    cardBG.alpha = 0.9
    cardBG.x = centerX
    cardBG.y = centerY + 23


    local foodTitle = display.newText( "Recommended Food List", display.contentCenterX, display.contentCenterY-180, customFont, 18 )
    foodTitle:setFillColor( 1,1,1,0.8 )

    --labelGrp = {"Lose Weight", "Gain Muscles", "Stay Fit", "Endurance"}
    goalTitle = display.newText( "", display.contentCenterX, foodTitle.y+30, customFont, 15 )
    goalTitle:setFillColor( 1,1,1,0.9 )

    local tableBG = display.newRect( display.contentCenterX, display.contentCenterY+10, display.contentWidth-80, display.contentCenterY+130 )
    tableBG:setFillColor( 0,0,0,0.8 )

    sceneGroup:insert(menubg)
    sceneGroup:insert(cardBG)
    sceneGroup:insert(tableBG)
    sceneGroup:insert(foodTitle)
    sceneGroup:insert(goalTitle)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        local personalInfo = loadsave.loadTable("personalData.json")
        getGoals()
        local myTblView = widget.newTableView(
            {
                left = display.contentCenterX-130,
                top = display.contentCenterY-110,
                height = 345,
                width = 260,
                friction = 0.1,
                onRowRender = onRowRender,
                onRowTouch = onRowTouch,
                isBounceEnabled = false,
                listener = scrollListener
            })

        path = system.pathForFile( "data.db", system.DocumentsDirectory )
        db = sqlite3.open( path )
        --local colName = {"WL","MG","EG","MW"}
        local colName = {"WL","MG","EG","MW"}
        local labelName = {"Lose Weight", "Gain Muscles","Endurance", "Stay Fit"}

        local myGoal

        for i = 1, #colName do
            if personalInfo.goalName == labelName[i] then
                myGoal = colName[i]
                break
            end
        end

        local sqlQuery = "SELECT * FROM FoodDB where purpose=".."\""..myGoal.."\""
        print(sqlQuery)
        --local sqlQuery = [[SELECT * FROM FoodDB where purpose ==]]
        for row in db:nrows(sqlQuery) do
            myTblView:insertRow{  rowHeight = 60, isCategory = false, params = {purpose=row.purpose,name=row.foodName}  }
            print(row.id,row.purpose,row.foodName)
        end

        db:close()

        sceneGroup:insert(myTblView)

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
