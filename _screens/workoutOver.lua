local composer = require( "composer" )
local widget = require("widget")
local sqlite3 = require( "sqlite3" )
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local menubg, cardBG, counterText, circleBG, circleText, durationText, infoDuration, infoExercise, superCount, swipeBox, memIndex

function switchCounter(incrementNo)
    if incrementNo == true and superCount < #infoExercise then
        --print("inrementing Counter")
        superCount = superCount + 1
    elseif incrementNo == false and superCount ~= 1 then
        superCount = superCount - 1
        --print("decrementing Counter"..superCount)
    end
    updateExercise()
    --print("current counter is "..superCount)
end

local function startDrag(event)
    local swipeLength = math.abs(event.x - event.xStart)
    --print(event.phase, swipeLength)
    local t = event.target
    local phase = event.phase
    if "began" == phase then
        return true
    elseif "moved" == phase then
    elseif "ended" == phase or "cancelled" == phase then
        if event.xStart > event.x and swipeLength > 50 then
            --print("Swiped Left Incremental")
            switchCounter(true)
        elseif event.xStart < event.x and swipeLength > 50 then
            --print( "Swiped Right Decremental" )
            switchCounter(false)
        end
    end
end

local function onKeyEvent( event )

    -- Print which key was pressed down/up
    local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    print( message )

    -- If the "back" key was pressed on Android, prevent it from backing out of the app
    if ( event.keyName == "back" ) then
        if ( system.getInfo("platform") == "android" ) then
            composer.hideOverlay()
            return true
        end
    end


    if ( event.keyName == "b" ) then
        composer.hideOverlay()
        return true
    end

    -- IMPORTANT! Return false to indicate that this app is NOT overriding the received key
    -- This lets the operating system execute its default handling of the key
    return false
end

local function initPage(keyIndex)

    local buttonGrp = {"Basic","Normal","Hardcore"}
    local path = system.pathForFile( "data.db", system.DocumentsDirectory )
    local db = sqlite3.open( path )
    local selective = {30,40,45}
    local count = 1


    local sqlQuery = "SELECT * FROM ExerciseDB where duration=".."\""..selective[keyIndex].."\""
    for row in db:nrows(sqlQuery) do
        infoExercise[count] = row.exercise
        infoDuration[count] = row.duration
        count = count+1
    end

    memIndex = keyIndex

    local userInfo = loadsave.loadTable("personalData.json")
    if userInfo.gender == "female" then
        infoDuration[memIndex] = math.round(infoDuration[memIndex] * ((100-23)/100))
    end

    updateExercise()

    db:close()


    print("my index "..keyIndex)

end

function updateExercise()
    counterText.text = "Exercise " ..superCount.." of " ..#infoExercise
    circleText.text = infoExercise[superCount]
    durationText.text = "Perform for ".. infoDuration[memIndex] .." Seconds"
end

function scene:create( event )

    local sceneGroup = self.view
    local myBtnGroup = display.newGroup()

    local customFont = composer.getVariable("universalFont")
    local defaultBG = composer.getVariable("universalBG")
    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight


    infoExercise = {}
    infoDuration = {}

    superCount = 1

    menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = display.contentCenterX
    menubg.y = display.contentCenterY + statusbarHeight

    --menubg = display.newRect( display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight )
    --menubg:setFillColor( 98/255,147/255,195/255,1 )

    cardBG = display.newImageRect( "assets/ui/card2.png", fullWidth,fullHeight)
    cardBG.alpha = 0.9
    cardBG.x = centerX
    cardBG.y = centerY + 10

    local custom =
        {
            text = "",
            width = 200,
            font = customFont,
            fontSize = 30,
            align = "center"  -- Alignment parameter
        }

    local options =
        {
            text = "",
            width = 250,
            font = customFont,
            fontSize = 30,
            align = "center"  -- Alignment parameter
        }

    counterText = display.newText( options )
    counterText.x = centerX
    counterText.y = centerY - 180
    counterText:setFillColor( 1, 0, 0,0.6 )

    circleBG = display.newCircle( counterText.x, counterText.y + 170, 100 )
    circleBG:setFillColor( 0.5 )

    circleText = display.newText(custom)
    circleText.text = ""
    circleText.x = circleBG.x
    circleText.y = circleBG.y

    durationText = display.newText(options)
    durationText.text = "Perform for 30 Seconds"
    durationText:setFillColor(0,0,0,0.8)
    durationText.size = 20
    durationText.x = circleText.x
    durationText.y = circleText.y + 200

    swipeBox = display.newRect(display.contentCenterX,display.contentCenterY,display.contentWidth,display.contentHeight)
    swipeBox:setFillColor(255,0,0,0.01)

    Runtime:addEventListener( "key", onKeyEvent )
    swipeBox:addEventListener("touch",startDrag)
    initPage(event.params.options)


end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

        Runtime:removeEventListener( "key", onKeyEvent )
        swipeBox:removeEventListener("touch",startDrag)

        menubg:removeSelf()
        menubg = nil

        cardBG:removeSelf()
        cardBG = nil

        counterText:removeSelf()
        counterText = nil

        circleBG:removeSelf()
        circleBG = nil

        circleText:removeSelf()
        circleText = nil

        durationText:removeSelf()
        durationText = nil

        swipeBox:removeSelf()
        swipeBox = nil

        composer.setVariable("overlay",false)

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
