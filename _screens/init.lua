-- screens.mainMenu

local composer = require ("composer")       -- Include the Composer library. Please refer to -> http://docs.coronalabs.com/api/library/composer/index.html
local scene = composer.newScene()           -- Created a new scene

local widget = require ("widget")     -- Included the Widget library for buttons, tabs, sliders and many more
local extendWidget = require("_Scripts.newPanel")
local extendNav = require("_Scripts.newNav")
local loadsave = require("_Scripts.loadsave")
require("_Scripts.datePickerWheel")

local widgets = {}
local pickerWheel, dataKeeper, qnText, submitBtn, btnGroup, labelGrp, switchScene

local function skipScene()
    local speed = composer.getVariable("speed")

    local options = {
        isModal = true,
        effect = "fade",
        time = speed
    }

    composer.gotoScene("_screens.loading", options)
end

local function excuteStartScene()
    local speed = composer.getVariable("speed", speed)
    
    local options = {
        isModal = true,
        effect = "fade",
        time = speed,
        params = {
            myData = dataKeeper
        }
    }

    -- By some method such as a "pause" button, show the overlay
    composer.showOverlay( "_screens.calibrationOverlay", options )
end

local function validateDate(m, d, y)
    local time = os.time({year=y, month=m, day=d})
    if time > os.time() then
        native.showAlert("Warning", "You cannot choose a date in the future.", {"OK"})
        return false
    end
    local date = os.date( "*t" )
    local age = date.year - y
    if age < 10 then
        native.showAlert("Warning", "You are too young to use this application!", {"OK"})
        return false
    end

    return true
end

local function retrieveData( event )
    if event.phase == "ended" then
        local values = pickerWheel:getValues()

        if not values[1] then
            native.showAlert("Warning!", "Please make sure a month is selected.", {"OK"})
            return
        elseif not values[2] then
            native.showAlert("Warning!", "Please make sure a day is selected.", {"OK"})
            return
        elseif not values[3] then
            native.showAlert("Warning!", "Please make sure a year is selected.", {"OK"})
            return
        end

        local valid = validateDate(values[1].index, values[2].value, values[3].value)

        if valid then
            -- Get the value for each column in the wheel, by column index
            local monthOB = values[1].value
            local dayOB = values[2].value
            local yearOB = values[3].value
    
            print(monthOB.."/"..dayOB.."/"..yearOB)
    
            local date = os.date( "*t" )    -- Returns table of date & time values
            dataKeeper.age = date.year-yearOB
            print("age is: " .. dataKeeper.age)
            switchARoo()
        end

    end
end

local function selectKeyBtn( event )
    if event.phase == "began" then
        --labelGrp = {"Lose Weight", "Gain Muscles", "Stay Fit", "Endurance"}
        print("current button clicked ".. event.target.id)
        dataKeeper.goal = event.target.id
        dataKeeper.goalName = labelGrp[event.target.id]
        dataKeeper.pushCounter = 0
        dataKeeper.runCounter = 0
        dataKeeper.sitUps = 0
        dataKeeper.weight = 0
        dataKeeper.height = 0
        dataKeeper.burpeesCounter = 0
        dataKeeper.gender = ""
        --loadsave.saveTable(dataKeeper, "personalData.json")
        excuteStartScene()
    end
end

function switchARoo()
    qnText.text = "What is your goal?"
    qnText.size = 30
    --qnText.y = display.contentCenterY - 200

    pickerWheel.isVisible = false
    submitBtn.isVisible = false

    for i = 1, #labelGrp do
        btnGroup[i].isVisible = true
    end
end

function scene:resumeProg()
    print("executed resume scene")
    composer.gotoScene("_screens.loading")
end


function scene:create( event )
    widget.setTheme( "widget_theme_android" )
    native.setActivityIndicator( state )
    local sceneGroup = self.view

    local defaultFont = composer.getVariable("universalFont")
    local defaultBG = composer.getVariable("universalBG")
    local statusBarHeight = composer.getVariable("statusHeight")
    local centerY = display.contentCenterY
    local centerX = display.contentCenterX

    composer.setVariable("overlay",false)
    dataKeeper = {}

    switchScene = 0

    local menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = centerX
    menubg.y = centerY + statusBarHeight

    local dataStorage = loadsave.loadTable("personalData.json")
    sceneGroup:insert(menubg)

    local textBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth+100,display.contentHeight-70)
    textBG.alpha = 0.85
    textBG:setFillColor(0,0,0,1)
    textBG.x = centerX
    textBG.y = centerY + 10
    sceneGroup:insert(textBG)

    if dataStorage == nil then
        print("first timer")
        local currYear = tonumber(os.date("%Y"))
        local currMonth = tonumber(os.date("%m"))
        local currDay = tonumber(os.date("%d"))
    
        -- Create the widget.
        pickerWheel = widget.newDatePickerWheel(currYear, currMonth, currDay)

        local options =
            {
                text = "Please enter your date of birth",
                width = display.contentWidth-30,
                font = defaultFont,
                fontSize = 19,
                align = "center"  -- Alignment parameter
            }

        qnText = display.newText( options )
        qnText:setFillColor( 1,1,1,0.9 )
        qnText.x = centerX
        qnText.y = centerY - 175

        pickerWheel.x = centerX - 160
        pickerWheel.y = centerY - 130

        submitBtn = widget.newButton(
            {
                left = 0,
                top = 0,
                id = "1",
                font = defaultFont,
                emboss = true,
                fontSize = 24,
                label = "Submit",
                labelColor = { default={ 1, 1, 1, 0.8 }, over={ 1, 1, 1, 0.5 } },
                onEvent = retrieveData
            })
        submitBtn:setFillColor(169/255,175/255,209/255,1)
        submitBtn.x = centerX
        submitBtn.y = centerY + 150--pickerWheel.y+ 150

        btnGroup = {}
        labelGrp = {"Lose Weight", "Gain Muscles", "Stay Fit", "Endurance"}

        sceneGroup:insert(qnText)

        for i = 1, #labelGrp do

            btnGroup[i] = widget.newButton({
                left = 0,
                top = 0,
                id = i,
                label = labelGrp[i],
                font = defaultFont,
                fontSize = 22,
                emboss = true,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 0.5 } },
                onEvent = selectKeyBtn,
                width = 210,
                height = 60
            })
            btnGroup[i]:setFillColor(169/255,175/255,209/255,1)
            btnGroup[i].y = (centerY-190) + (i * 90)
            btnGroup[i].x = centerX

            btnGroup[i].isVisible = false
            sceneGroup:insert(btnGroup[i])
        end

        sceneGroup:insert(pickerWheel)
        sceneGroup:insert(submitBtn)

    else
        switchScene = 1
        skipScene()
    end
    native.setActivityIndicator( false )
    --excuteStartScene()

end


function scene:show( event )
    local phase = event.phase

    if ( phase == "will" ) then         -- Scene is not shown entirely


    elseif ( phase == "did" ) then      -- Scene is fully shown on the screen


    end
end


function scene:hide( event )
    local phase = event.phase

    if ( phase == "will" ) then         -- Scene is not off the screen entirely

    elseif ( phase == "did" ) then      -- Scene is off the screen



    end
end

function scene:destroy( event )
-- Called before the scene is removed
end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene
