local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local maleBtn, femaleBtn, eventData, titleText

local heightField, weightField

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
        -- Do nothing; dialog will simply dismiss
        end
    end
end

local function textListener( event )

    if ( event.phase == "began" ) then
    -- User begins editing "defaultField"

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
        -- Output resulting text from "defaultField"
        --height
        --print( event.target.text )
        eventData.height = tonumber(event.target.text)

    elseif ( event.phase == "editing" ) then
        eventData.height = tonumber(event.text)
    end
end

local function textListener2( event )

    if ( event.phase == "began" ) then
    -- User begins editing "defaultField"

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
        -- Output resulting text from "defaultField"
        --weight
        ---print( event.target.text )
        eventData.weight = tonumber(event.target.text)

    elseif ( event.phase == "editing" ) then
        --print( event.newCharacters )
        --print( event.startPosition )
        --print( event.text )
        eventData.weight = tonumber(event.text)
    end
end

function dashScene(event)

    if event.phase == "ended" then
        print(eventData.weight)
        if eventData.weight == 0 or eventData.height == 0 or eventData.weight == nil or eventData.height == nil then
            native.showAlert( "Warning", "Please fill up weight and height fields", { "OK" }, onComplete )
            print("error")
        elseif eventData.gender == nil or eventData.gender == "" then
            print("error2")
            native.showAlert( "Warning", "Please select your gender", { "OK" }, onComplete )
        else
            loadsave.saveTable(eventData, "personalData.json")
            composer.setVariable("overlay",false)
            composer.hideOverlay()
        end

    end
    --loadsave.saveTable(eventData, "personalData.json")

end

local function handleEvents(event)
    if event.phase == "began" then
        if event.target.id == "male" then
            femaleBtn:setFillColor(1,1,1,0.6)
            maleBtn:setFillColor(0,0,0,0.5)
            eventData.gender = "male"
        elseif event.target.id == "female" then
            femaleBtn:setFillColor(0,0,0,0.5)
            maleBtn:setFillColor(1,1,1,0.6)
            eventData.gender = "female"
        end
    end

end

function scene:create( event )

    local sceneGroup = self.view

    local centerY = display.contentCenterY
    local centerX = display.contentCenterX
    
    eventData = event.params.myData

    local defaultBG = composer.getVariable("universalBG")
    local customFont = composer.getVariable("universalFont")

    local menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = display.contentCenterX
    menubg.y = display.contentCenterY

    local cardBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth,display.contentHeight + 50)
    cardBG.alpha = 0.9
    cardBG.x = display.contentCenterX
    cardBG.y = display.contentCenterY


    --display text, button and textfields
    local options =
        {
            text = "Basic Info",
            width = display.contentWidth-30,
            font = defaultFont,
            fontSize = 23,
            align = "center"  -- Alignment parameter
        }

    titleText = display.newText( options )
    titleText:setFillColor( 0 )
    titleText.x = display.contentCenterX
    titleText.y = display.contentCenterY - 220

    femaleBtn = widget.newButton({
        left = 0,
        top = 0,
        id = "female",
        label = "Female",
        font = customFont,
        fontSize = 18,
        width = centerX - 30,
        height = 50,
        onEvent = handleEvents
    })

    femaleBtn:setFillColor(1,1,1,0.6)
    femaleBtn.x = display.contentCenterX - 80
    femaleBtn.y = titleText.y + 70

    maleBtn = widget.newButton({
        left = 0,
        top = 0,
        id = "male",
        label = "Male",
        font = customFont,
        fontSize = 18,
        width = centerX - 30,
        height = 50,
        onEvent = handleEvents
    })
    maleBtn:setFillColor(1,1,1,0.6)
    maleBtn.x = display.contentCenterX + 80
    maleBtn.y = femaleBtn.y

    customFont = composer.getVariable("universalFont")
    local defaultColorScheme = { 14/255,28/255,54/255,1 }

    local options =
        {
            text = "Your Height (cm)",
            width = display.contentWidth-100,
            font = defaultFont,
            fontSize = 19,
            align = "left"  -- Alignment parameter
        }

    local heightText = display.newText( options )
    heightText:setFillColor( 0 )
    heightText.x = display.contentCenterX
    heightText.y = display.contentCenterY - 80

    heightField = native.newTextField( display.contentCenterX, heightText.y + 50, display.contentWidth - 100, 40 )
    heightField.inputType = "number"
    heightField.hasBackground = false
    heightField:addEventListener( "userInput", textListener )

    local heightLine = display.newRect( display.contentCenterX, heightField.y + 30, display.contentWidth-100, 3 )
    heightLine:setFillColor( unpack(defaultColorScheme) )

    local weightText = display.newText( options )
    weightText.text = "Your Weight (kg)"
    weightText:setFillColor( 0 )
    weightText.x = display.contentCenterX
    weightText.y = heightLine.y + 50

    weightField = native.newTextField( display.contentCenterX, weightText.y + 50, display.contentWidth - 100, 40 )
    weightField.inputType = "number"
    weightField.hasBackground = false
    weightField:addEventListener( "userInput", textListener2 )

    local weightLine = display.newRect( display.contentCenterX, weightField.y + 30, display.contentWidth-100, 3 )
    weightLine:setFillColor( unpack(defaultColorScheme) )

    local confirmBtn = widget.newButton(
        {
            left = 0,
            top = 0,
            id = "1",
            label = "Submit",
            onEvent = dashScene
        })
    confirmBtn.x = display.contentCenterX
    confirmBtn.y = weightLine.y+ 100


    sceneGroup:insert(menubg)
    sceneGroup:insert(cardBG)
    sceneGroup:insert(femaleBtn)
    sceneGroup:insert(maleBtn)
    sceneGroup:insert(titleText)
    sceneGroup:insert(heightText)
    sceneGroup:insert(heightLine)
    sceneGroup:insert(weightText)
    sceneGroup:insert(weightLine)
    sceneGroup:insert(confirmBtn)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then



    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        event.parent:resumeProg()

    elseif ( phase == "did" ) then
        heightField:removeSelf()
        heightField = nil

        weightField:removeSelf()
        weightField = nil



    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
