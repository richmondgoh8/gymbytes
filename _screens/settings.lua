local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local btnGroup

local function selectColor(event)
    local userInfo = loadsave.loadTable("personalData.json")
    local labelGrp = {"Lose Weight", "Gain Muscles", "Stay Fit", "Endurance"}
    local updatedData = userInfo
    for i = 1, 4 do
        if i == event.target.id then
            btnGroup[i]:setFillColor(0,0,0,0.5)
            updatedData.goal = event.target.id
            updatedData.goalName = labelGrp[event.target.id]
            loadsave.saveTable(updatedData, "personalData.json")
        else
            btnGroup[i]:setFillColor(1,1,1,0.2)
        end
    end
end

local function resetApp(event)
    if event.phase == "began" then
        if event.target.id == 0 then --reset
            local options =
                {
                    to = "richmondgoh@gmail.com",
                    subject = "Urgent Bug Report!",
                    body = ""
                }
        native.showPopup( "mail", options )
        elseif event.target.id == 1 then
            system.openURL( "https://www.paypal.me/RichmondGoh" )
        end
    end
end

function scene:create( event )

    local sceneGroup = self.view

    local customFont = composer.getVariable("universalFont")
    local defaultBG = composer.getVariable("universalBG")

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight

    local menubg = display.newImageRect( defaultBG, fullWidth,fullHeight)
    menubg.x = centerX
    menubg.y = centerY + statusbarHeight

    local cardBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth,display.contentHeight - 40)
    cardBG.alpha = 0.9
    cardBG.x = display.contentCenterX
    cardBG.y = display.contentCenterY + 30

    local options =
        {
            text = "",
            width = display.contentWidth,
            font = customFont,
            fontSize = 28,
            align = "center"  -- Alignment parameter
        }

    local goalText = display.newText( options )
    goalText.x = display.contentCenterX
    goalText.y = display.contentCenterY - 170
    goalText.text = "Select a New Goal"
    goalText:setFillColor(0)

    local group = display.newGroup()
    btnGroup = {}
    local labelGrp = {"Lose Weight", "Gain Muscles", "Stay Fit", "Endurance"}
    local cellCount = 0
    local cellRow = 1


    for i = 1, #labelGrp do
        print(cellCount)
        xAxis = (cellCount + 1) * 150 - 47
        yAxis = (display.contentCenterY + cellRow*60 - 225)+70

        btnGroup[i] = widget.newButton({
            left = 0,
            top = 0,
            id = i,
            label = labelGrp[i],
            font = customFont,
            fontSize = 17,
            onEvent = selectColor,
            width = 120,
            height = 60
        })
        btnGroup[i].y = yAxis
        btnGroup[i].x = xAxis
        cellCount = cellCount + 1

        if cellCount == 2 then
            cellCount = 0
            cellRow = cellRow + 1
        end
        btnGroup[i].isVisible = true
        group:insert(btnGroup[i])
    end

    local resetBtn = widget.newButton({
        left = 0,
        top = 0,
        id = 0,
        label = "Send Bug Report",
        font = customFont,
        fontSize = 19,
        onEvent = resetApp,
        width = 250,
        height = 70
    })

    resetBtn.x = display.contentCenterX
    resetBtn.y = display.contentCenterY + 70

    local donateBtn = widget.newButton({
        left = 0,
        top = 0,
        id = 1,
        font = customFont,
        fontSize = 19,
        label = "Donate to Developer",
        onEvent = resetApp,
        width = 250,
        height = 70
    })

    donateBtn.x = display.contentCenterX
    donateBtn.y = display.contentCenterY + 160

    sceneGroup:insert(menubg)
    sceneGroup:insert(cardBG)
    sceneGroup:insert(goalText)
    sceneGroup:insert(group)
    sceneGroup:insert(resetBtn)
    sceneGroup:insert(donateBtn)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

        local userInfo = loadsave.loadTable("personalData.json")

        for i = 1, 4 do
            if i == userInfo.goal then
                btnGroup[i]:setFillColor(0,0,0,0.5)
            else
                btnGroup[i]:setFillColor(1,1,1,0.6)
            end
        end

    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
