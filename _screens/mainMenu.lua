local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local heartText, square, arrowIcon, helperTxt, fitnessTxt

local function overlayPage(event)
    if event.phase == "began" then
        local options = {
            isModal = true,
            effect = "fade",
            time = 400,
        }
        -- By some method such as a "pause" button, show the overlay
        composer.setVariable("overlay",true)
        composer.showOverlay( "_screens.adjustInfo", options )
    end
end

function scene:overlayBack()
    composer.gotoScene("_screens.mainMenu")
end

local function getFitnessLevel()
    print("calculating fitness level...")
    local userInfo = loadsave.loadTable("personalData.json")
    local runningTime = (userInfo.runCounter%1*100) + (math.floor(userInfo.runCounter)*60)
    local pushUps = userInfo.pushCounter
    local firstStr = ""
    local secondStr = ""

    print(runningTime,pushUps)
    print("Total Running Time: (Sec) " .. runningTime)

    if runningTime >= 60*12 then
        print("slow")
        firstStr = "Slow"

    elseif runningTime >= 60 * 10 then
        print("ordinary")
        firstStr = "Decent"

    elseif runningTime == 0 then
        firstStr = "N.A."
        print("uncalibrated")

    else
        print("fast")
        firstStr = "Fast"
    end

    if pushUps >= 40 then
        print("Strong")
        secondStr = "Strong"
    elseif pushUps >= 26 then
        secondStr = "Normal"
    elseif pushUps == 0 then
        secondStr = "N.A."
    else
        print("weak")
        secondStr = "Weak"
    end

    if firstStr == "" and secondStr == "" then
        fitnessTxt.text = "N.A."
    else
        fitnessTxt.text = firstStr.." & "..secondStr
    end
end

local function getBMI()
    local userInfo = loadsave.loadTable("personalData.json")
    local height = userInfo.height/100
    local weight = userInfo.weight --weight in meter
    local bmi = weight / (height*height)
    local min = 15
    local max = 40
    --local newBMI = (16/33)*180
    local newBMI = (bmi - min) * (180 / (max-min))

    arrowIcon.rotation = 0

    arrowIcon.rotation = arrowIcon.rotation + newBMI

    if bmi <= 15 then
        arrowIcon.rotation = 0
    end

    if bmi >= 40 then
        arrowIcon.rotation = 0
    end

    bmi = math.round(bmi*10)*0.10
    helperTxt.text = "BMI ("..bmi..")"
    --print(bmi,height,weight,newBMI)
end

local function arc(x, y, radius, theta1, theta2, width, detail)
    local vertices = {};
    local k = 1;

    for i = (360 - theta2), (360 - theta1), detail
    do
        vertices[k] = radius*math.cos(2*math.pi*(i / 360));
        vertices[k + 1] = radius*math.sin(2*math.pi*(i / 360));
        k = k + 2;
    end
    for i = ((360 - theta1) - ((360 - theta1) % detail)), (360 - theta2), -detail
    do
        vertices[k] = (radius - width)*math.cos(2*math.pi*(i / 360));
        vertices[k + 1] = (radius - width)*math.sin(2*math.pi*(i / 360));
        k = k + 2;
    end

    return display.newPolygon(x, y, vertices), vertices;
end

local function getHeartZone()
    local userInfo = loadsave.loadTable("personalData.json")
    local gender = userInfo.gender
    local age = userInfo.age
    local maxRate

    if gender == "male" then
        maxRate = 202 - 0.55 * age
        return math.round((55+85)/200 * maxRate)

    elseif gender == "female" then
        maxRate = 216 - 1.09 * age
        return math.round((55+85)/200 * maxRate)
    end
end

function scene:create( event )

    local sceneGroup = self.view

    local baseHeight = 20
    local customFont = composer.getVariable("universalFont")
    local statusbarHeight = composer.getVariable("statusHeight")
    local defaultBG = composer.getVariable("universalBG")
    local navBarHeight = composer.getVariable("navBar")

    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight
    
    local menubg = display.newImageRect( defaultBG, fullWidth,fullHeight)
    menubg.x = centerX
    menubg.y = centerY + statusbarHeight

    local textBG = display.newImageRect( "assets/ui/card2.png", fullWidth,centerY)
    textBG.alpha = 1
    textBG.x = centerX
    textBG.y = centerY - 80

    local textBG2 = display.newImageRect( "assets/ui/card2.png", fullWidth,centerY-30)
    textBG2.alpha = 1
    textBG2.x = centerX
    textBG2.y = textBG.y * 2 + 40

    local heartIcon = display.newImageRect( "assets/ui/heart.png", 100,85 )
    heartIcon.alpha = 0.9
    heartIcon.x = centerX
    heartIcon.y = textBG.y/2 + 35

    heartText = display.newText( "", heartIcon.x, heartIcon.y-5, customFont, 24 )
    heartText:setFillColor( 0.8, 0.1, 0.4 )

    local helperText = display.newText( "Optimal Training Heart Zone", heartIcon.x, heartIcon.y+heartIcon.height/1.5, customFont, 15 )
    helperText:setFillColor( 0.8, 0.1, 0.4 )

    --x, y, radius, theta1, theta2, width, detail
    local safeArc, v = arc(0, 0, 100, 0, 180, 10, 1);
    safeArc.strokeWidth = 3;
    safeArc:setStrokeColor( 0.8, 0, 0.2 );
    safeArc.fill = {0.8, 0, 0.2};

    local midArc, v = arc(0, 0, 100, 45, 83, 10, 1);
    midArc:rotate(-61)
    midArc.x = -50
    midArc.y = -26
    midArc.strokeWidth = 4;
    midArc:setStrokeColor( 0.5, 1, 0.5 );
    midArc.fill = {0.5, 1, 0.5};

    shapeGroup = display.newGroup();
    shapeGroup:insert(safeArc);
    shapeGroup:insert(midArc);

    shapeGroup.x = centerX;
    shapeGroup.y = centerY - 30;


    local pointerCircle = display.newCircle(display.contentCenterX, shapeGroup.y + 40, 7);
    pointerCircle.fill = {1, 0, 0};

    helperTxt = display.newText( "BMI", heartIcon.x, shapeGroup.y + 60, customFont, 15 )
    helperTxt:setFillColor( 0.8, 0.1, 0.4 )

    --local arrowIcon = display.newImageRect( "assets/ui/arrow.png", 50,50)
    arrowIcon = display.newRect( 0, 0, -80, 5 )
    arrowIcon.alpha = 1
    --arrowIcon.x = pointerCircle.x + pointerCircle.width*1.8
    --arrowIcon.y = pointerCircle.y - pointerCircle.height/2
    arrowIcon.anchorX = 0
    arrowIcon.anchorY = 0
    arrowIcon.yReference = 0
    arrowIcon.x = pointerCircle.x
    arrowIcon.y = pointerCircle.y - 5
    arrowIcon:setFillColor(0)

    local card2helpTxt = display.newText( "Overall Fitness Level:", heartIcon.x, textBG2.y-60, customFont, 25 )
    card2helpTxt:setFillColor( 0.8, 0.1, 0.4 )

    -- 3 measures - weak, normal, strong
    fitnessTxt = display.newText( "", heartIcon.x, card2helpTxt.y + 30, customFont, 16 )
    fitnessTxt:setFillColor( 0.8, 0.1, 0.4 )

    local calibrateBtn = widget.newButton(
        {
            width = 250,
            height = 60,
            label = "Re-Calibrate",
            font = customFont,
            fontSize = 25,
            emboss = true,
            labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 0.5 } },
            onEvent = overlayPage
        })

    -- Center the button
    calibrateBtn:setFillColor(169/255,175/255,209/255,1)
    calibrateBtn.x = display.contentCenterX
    calibrateBtn.y = fitnessTxt.y + 70

    sceneGroup:insert(menubg)
    sceneGroup:insert(textBG)
    sceneGroup:insert(textBG2)
    sceneGroup:insert(heartIcon)
    sceneGroup:insert(heartText)
    sceneGroup:insert(helperText)
    sceneGroup:insert(helperTxt)
    sceneGroup:insert(arrowIcon)
    sceneGroup:insert(pointerCircle)
    sceneGroup:insert(card2helpTxt)
    sceneGroup:insert(fitnessTxt)
    sceneGroup:insert(shapeGroup)
    sceneGroup:insert(calibrateBtn)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

        heartText.text = getHeartZone()
        getBMI()
        getFitnessLevel()


    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
