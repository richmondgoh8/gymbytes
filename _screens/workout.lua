local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
        -- Do nothing; dialog will simply dismiss
        end
    end
end

local function ripButton( event )
    if event.phase == "began" then
        local personalInfo = loadsave.loadTable("personalData.json")
        print(event.target.id)
        if personalInfo.pushCounter == 0 then
            native.showAlert( "Warning", "Please Calibrate your stats on Dashboard", { "OK" }, onComplete )
        else
            composer.setVariable("overlay",true)
            local options = {
                isModal = true,
                effect = "fade",
                time = 400,
                params = {
                    options = event.target.id
                }}

            composer.showOverlay( "_screens.workoutOver", options )
        end
    end
end

function scene:create( event )

    local sceneGroup = self.view
    local myBtnGroup = display.newGroup()

    local customFont = composer.getVariable("universalFont")
    local defaultBG = composer.getVariable("universalBG")

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight

    local menubg = display.newImageRect( defaultBG, fullWidth,fullHeight)
    menubg.x = centerX
    menubg.y = centerY + statusbarHeight

    local cardBG = display.newImageRect( "assets/ui/card2.png", fullWidth,fullHeight - 80)
    cardBG.alpha = 0.9
    cardBG.x = display.contentCenterX
    cardBG.y = display.contentCenterY + 30

    local options =
    {
        text = "Personal Training Programmes",
        x = centerX,
        y = centerY-125,
        width = 300,
        font = customFont,
        fontSize = 25,
        align = "center"  -- Alignment parameter
    }

    local myTitle = display.newText( options )
    myTitle:setFillColor( 0.8, 0.1, 0.4 )

    local intensityTxt = display.newText(options)
    intensityTxt.text = "Choose your Intensity"--Basic, Normal, Hardcore
    intensityTxt.size = 20
    intensityTxt.y = centerY-60
    intensityTxt:setFillColor( 0.8, 0.1, 0.4 )

    --init buttons
    local buttonGrp = {"Basic","Normal","Hardcore"}
    local buttonArr = {}
    for i = 1, #buttonGrp do

        buttonArr[i] = widget.newButton(
            {
                width = 200,
                height = 50,
                id = i,
                font = customFont,
                fontSize = 20,
                label = buttonGrp[i],
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 0.5 } },
                onEvent = ripButton
            })

        buttonArr[i].x = display.contentCenterX
        buttonArr[i].y = 40 + (display.contentCenterY -100) + (i*70)
        buttonArr[i]:setFillColor(169/255,175/255,209/255,1)

        myBtnGroup:insert(buttonArr[i])


        -- Center the button

    end

    sceneGroup:insert(menubg)
    sceneGroup:insert(cardBG)
    sceneGroup:insert(myTitle)
    sceneGroup:insert(intensityTxt)
    sceneGroup:insert(myBtnGroup)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
