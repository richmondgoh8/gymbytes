local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local menubg, cardBG, titleText, trackBtn
local stepsText, distText
local stepCounter, distanceCounter
local amplifer
local userInfo
local meterOn
local timerID, timeElapsed

local holder

local function startTimer( event )
    timeElapsed = timeElapsed + 1
end

local function initStride()
    if userInfo.gender == "male" then
        amplifer = 0.415
    elseif userInfo.gender == "female" then
        amplifer = 0.413
    end
end

local function takenSteps( event )
    if event.isShake then
        stepCounter = stepCounter + 1
        print( "The device is being shaken!" )
        print( stepCounter )
    end
    return true
end

local function changeFunction(event)
    local mySwitch = {"Start","Stop"}
    local myLabel = trackBtn:getLabel()

    if event.phase == "began" and myLabel == mySwitch[1] then
        trackBtn:setLabel( mySwitch[2] )
        stepCounter = 0
        timeElapsed = 0
        meterOn = 1
        timerID = timer.performWithDelay( 1000, startTimer, -1 )
        Runtime:addEventListener( "accelerometer", takenSteps )
     elseif event.phase == "began" and myLabel == mySwitch[2] then
        trackBtn:setLabel(mySwitch[1])
        Runtime:removeEventListener( "accelerometer", takenSteps )
        meterOn = 0
        timer.cancel(timerID)
        local accurateMeter = (stepCounter * userInfo.height * amplifer)/100
        local speed = accurateMeter/timeElapsed

        if timeElapsed <= 1 then
            speed = 0 
        end
        distText.text = "Your Calculated distance is \n" .. math.round(accurateMeter) .. " m"
        stepsText.text = "Your current speed is: \n".. string.format("%.2f", speed) .. " m/s"
    end

end

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
        -- Do nothing; dialog will simply dismiss
        end
    end
end

function scene:create( event )

    local sceneGroup = self.view

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight

    local defaultBG = composer.getVariable("universalBG")
    local customFont = composer.getVariable("universalFont")
    local defaultColorScheme = { 14/255,28/255,54/255,1 }

    userInfo = loadsave.loadTable("personalData.json")
    initStride()

    stepCounter = 0
    distanceCounter = 0
    meterOn = 0

    menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = display.contentCenterX
    menubg.y = display.contentCenterY + statusbarHeight

    cardBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth,display.contentHeight - 40)
    cardBG.alpha = 0.9
    cardBG.x = display.contentCenterX
    cardBG.y = display.contentCenterY + 30


    --display text, button and textfields
    local options =
        {
            text = "Stats Tracker",
            width = display.contentWidth-30,
            font = defaultFont,
            fontSize = 23,
            align = "center"  -- Alignment parameter
        }

    titleText = display.newText( options )
    titleText:setFillColor( 0 )
    titleText.x = display.contentCenterX
    titleText.y = display.contentCenterY - 140

    stepsText = display.newText( options )
    stepsText.text = "Your current speed is: __"
    stepsText.size = 19
    stepsText:setFillColor( 0 )
    stepsText.x = display.contentCenterX
    stepsText.y = display.contentCenterY - 30

    distText = display.newText( options )
    distText.text = "Your Calculated distance is __"
    distText.size = 19
    distText:setFillColor( 0 )
    distText.x = display.contentCenterX
    distText.y = stepsText.y + 100

    trackBtn = widget.newButton({
        left = 0,
        top = 0,
        id = "SS",
        label = "Start",
        font = customFont,
        fontSize = 18,
        defaultFile = "assets/ui/genericButton.png",
        overFile = "assets/ui/genericButton.png",
        width = 150,
        height = 50,
        onEvent = changeFunction
    })
    trackBtn.x = display.contentCenterX
    trackBtn.y = display.contentCenterY+200

    holder = {menubg,cardBG,titleText,stepsText, distText, trackBtn}

    sceneGroup:insert(menubg)
    sceneGroup:insert(cardBG)
    sceneGroup:insert(titleText)
    sceneGroup:insert(stepsText)
    sceneGroup:insert(distText)
    sceneGroup:insert(trackBtn)


end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        composer.setVariable("overlay",false)
        print("hide scene")
        if meterOn == 1 then
            Runtime:removeEventListener( "accelerometer", takenSteps )
            timer.cancel(timerID)
        end
        Runtime:removeEventListener( "key", onKeyEvent )

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
