local composer = require( "composer" )
local widget = require("widget")
local gym = require("_places.gym")
local clinic = require("_places.clinic")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local cos,sin,pi,sqrt,min,asin,abs, locationNumber
local currentLocation, currentLatitude, currentLongitude

local function onKeyEvent( event )

    -- Print which key was pressed down/up
    local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    print( message )

    -- If the "back" key was pressed on Android, prevent it from backing out of the app
    if ( event.keyName == "back" ) then
        if ( system.getInfo("platform") == "android" ) then
            composer.hideOverlay()
            return true
        end
    end


    if ( event.keyName == "b" ) then
        composer.hideOverlay()
        return true
    end

    -- IMPORTANT! Return false to indicate that this app is NOT overriding the received key
    -- This lets the operating system execute its default handling of the key
    return false
end

function filterLocation(aIndex)
     if aIndex == 1 then --gym
        for i, Prop in ipairs(gym) do
            Prop['distance'] = distance(centerPoint, gym[i])
        end

        table.sort(gym, compare)

        for i = 1, 3 do
            local Displaytitle = gym[i].title
            local lat = gym[i].lat
            local lng = gym[i].lng

            local options = {
                title = Displaytitle
            }

            myMap:addMarker(lat, lng, options)
        end

     elseif aIndex == 2 then --clinic
        for i, Prop in ipairs(clinic) do
            Prop['distance'] = distance(centerPoint, clinic[i])
        end

        table.sort(clinic, compare)

        for i = 1, 3 do
            local Displaytitle = clinic[i].title
            local lat = clinic[i].lat
            local lng = clinic[i].lng

            local options = {
                title = Displaytitle
            }

            myMap:addMarker(lat, lng, options)
        end

     end
end

local function locateFacilities(event)
    if event.phase == "began" and event.target.id == 3 then
        if myMap then
            myMap:removeAllMarkers()
            locationNumber = 1 -- reset counter for popup labels
        end
    end

    if event.phase == "began" and event.target.id ~= 3 then

        currentLocation = myMap:getUserLocation()
        if currentLocation.errorCode then
            if currentLocation.errorCode ~= 0 then -- errorCode 0 is: Pending User Authorization!
            currentLatitude = 0
            currentLongitude = 0
            native.showAlert( "Error", currentLocation.errorMessage, { "OK" } )
            end
        else
        -- Current location data was received.
        -- Move map so that current location is at the center.
            myMap:removeAllMarkers()
            locationNumber = 1 -- reset counter for popup labels
            currentLatitude = currentLocation.latitude
            currentLongitude = currentLocation.longitude
            centerPoint = {lat = currentLatitude, lng = currentLongitude}
            myMap:setRegion( currentLatitude, currentLongitude, 0.01, 0.01, true )
    
            -- Look up nearest address to this location (this is returned as a "mapAddress" event, handled above)
            myMap:nearestAddress( currentLatitude, currentLongitude, mapAddressHandler )
    
            --Add marker for gym
            centerPoint = {lat = currentLatitude, lng = currentLongitude}
            filterLocation(event.target.id)
        end
    end
end


function mapAddressHandler( event )
    if event.isError then
        -- Failed to receive location information.
        native.showAlert( "Error", event.errorMessage, { "OK" } )
    else
        -- Location information received. Display it.
        local locationText =
            --"Latitude: " .. currentLatitude ..
            --", Longitude: " .. currentLongitude ..
            "Address: " .. ( event.streetDetail or "" ) ..
            " " .. ( event.street or "" ) ..
            ", " .. ( event.city or "" ) ..
            ", " .. ( event.region or "" ) ..
            ", " .. ( event.country or "" ) ..
            ", " .. ( event.postalCode or "" )
        native.showAlert( "You Are Here", locationText, { "OK" } )
    end
end

local function initMap()
    if myMap then
    -- Display a normal map with vector drawings of the streets.
    -- Other mapType options are "satellite" and "hybrid".
    myMap.mapType = "normal"

    -- The MapView is just another Corona display object and can be moved, resized, etc.
    myMap.x = display.contentWidth / 2
    myMap.y = display.contentCenterY + 19
    -- Initialize map to a real location, since default location (0,0) is not very interesting
    myMap:setCenter( 1.2874764, 103.8264809 )
    end
end

function distance(from, to)
    local distance = 0
    local radius = 6367000
    local radian = pi / 180
    local deltaLatitude = sin(radian * (from.lat - to.lat) /2)
    local deltaLongitude = sin(radian * (from.lng - to.lng) / 2)

    local circleDistance = 2 * asin(min(1, sqrt(deltaLatitude * deltaLatitude +
        cos(radian * from.lat) * cos(radian * to.lat) * deltaLongitude * deltaLongitude)))
    distance = abs(radius * circleDistance)
    return distance
end

-- Sort by distance
function compare(a,b)
    return a['distance'] < b['distance']
end

function scene:create( event )

    local sceneGroup = self.view

    local defaultBG = composer.getVariable("universalBG")
    local customFont = composer.getVariable("universalFont")

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight


    local menubg = display.newImageRect( defaultBG, fullWidth,fullHeight)
    menubg.x = centerX
    menubg.y = centerY + statusbarHeight

    local gymBtn = widget.newButton {
            label = "Find Nearby Gym",
            font = customFont,
            fontSize = 18,
            width = centerX + 5,
            height = 60,
            labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 0.5 } },
            emboss = true,
            id = 1,
            onEvent = locateFacilities,
    }
    
    local clinicBtn = widget.newButton {
            label = "Find Nearby Clinic",
            emboss = true,
            font = customFont,
            fontSize = 15,
            fontSize = 18,
            labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 0.5 } },
            width = centerX + 5,
            height = 60,
            id = 2,
            onEvent = locateFacilities,
    }
    
    gymBtn.x = gymBtn.width/2
    gymBtn.y = fullHeight-26
    clinicBtn.x = gymBtn.x *2 + 82
    clinicBtn.y = gymBtn.y

    gymBtn:setFillColor(169/255,175/255,209/255,1)
    clinicBtn:setFillColor(169/255,175/255,209/255,1)

    sceneGroup:insert(menubg)
    sceneGroup:insert(gymBtn)
    sceneGroup:insert(clinicBtn)

    Runtime:addEventListener( "key", onKeyEvent )

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    myMap = native.newMapView( 20, 20, display.contentWidth-10, display.contentCenterY+180 )
    initMap()

    if not myMap then
        myRectangle = display.newRect( 20, 20, display.contentWidth-10, display.contentCenterY+180 )
        myRectangle.x = display.contentWidth / 2
        myRectangle.y = display.contentCenterY + 19
        myRectangle:setFillColor(0,0,0,0.6)
        sceneGroup:insert(myRectangle)
    end

    cos = math.cos
    sin = math.sin
    pi = math.pi
    sqrt = math.sqrt
    min = math.min
    asin = math.asin
    abs = math.abs


    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

        composer.setVariable("overlay",false)
        Runtime:removeEventListener( "key", onKeyEvent )

        if myMap then
            myMap:removeSelf()
            myMap = nil
        end

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
