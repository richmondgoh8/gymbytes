local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local menubg, titleText, confirmBtn, cardBG
local heightText,heightLine,heightField
local weightText, weightLine, weightField
local pushUpText, pushUpLine, pushUpField
local runText, runField, runField2, runLine, runLine2

local function onKeyEvent( event )

    -- Print which key was pressed down/up
    local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    print( message )

    -- If the "back" key was pressed on Android, prevent it from backing out of the app
    if ( event.keyName == "back" ) then
        if ( system.getInfo("platform") == "android" ) then
            composer.hideOverlay()
            return true
        end
    end


    if ( event.keyName == "b" ) then
        composer.hideOverlay()
        return true
    end

    -- IMPORTANT! Return false to indicate that this app is NOT overriding the received key
    -- This lets the operating system execute its default handling of the key
    return false
end

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
        -- Do nothing; dialog will simply dismiss
        end
    end
end

local function textListener( event )

    if ( event.phase == "began" ) then
    -- User begins editing "defaultField"

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
    -- Output resulting text from "defaultField"

    elseif ( event.phase == "editing" ) then
    --eventData.height = tonumber(event.text)
    end
end

function backScene(event)

    if event.phase == "ended" then
        local userInfo = loadsave.loadTable("personalData.json")
        --composer.hideOverlay()

        local runMin = 0
        local runSec = 0

        if userInfo.runCounter ~= 0 then
            runMin = math.floor(userInfo.runCounter)
            runSec = userInfo.runCounter%1*100
            print(runSec.."mysec")
        end

        --print(pushUpField.text,weightField.text,heightField.text,runField.text,runField2.text)

        if heightField.text ~= "" then
            userInfo.height = tonumber(heightField.text)
        end

        if weightField.text ~= "" then
            userInfo.weight = tonumber(weightField.text)
        end

        if pushUpField.text ~= "" then
            userInfo.pushCounter = tonumber(pushUpField.text)
        end

        if runField.text ~= "" then
            runMin = tonumber(runField.text)
        end

        if runField2.text ~= "" then
            runSec = tonumber(runField2.text)
        end

        if runField2.text ~= "" and tonumber(runField2.text) > 60 then
            native.showAlert( "Warning", "Seconds cannot be more than 60!", { "OK" }, onComplete )
            return
        end

        userInfo.runCounter = runMin + (runSec/100)
        loadsave.saveTable(userInfo, "personalData.json")
        composer.hideOverlay()
    end


end

function scene:create( event )

    local sceneGroup = self.view

    local userInfo = loadsave.loadTable("personalData.json")
    local customFont = composer.getVariable("universalFont")
    local standardFontSize = 16
    local standardBoxWidth = display.contentWidth-100

    local statusbarHeight = composer.getVariable("statusHeight")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local fullWidth = display.contentWidth
    local fullHeight = display.contentHeight

    local defaultColorScheme = { 14/255,28/255,54/255,1 }

    local defaultBG = composer.getVariable("universalBG")

    menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = display.contentCenterX
    menubg.y = display.contentCenterY + statusbarHeight

    cardBG = display.newImageRect( "assets/ui/card2.png", display.contentWidth,display.contentHeight+40)
    cardBG.alpha = 1
    cardBG.x = display.contentCenterX
    cardBG.y = display.contentCenterY

    --display text, button and textfields
    local options =
        {
            text = "Calibration Page",
            width = display.contentWidth-30,
            font = defaultFont,
            fontSize = 25,
            align = "center"  -- Alignment parameter
        }

    titleText = display.newText( options )
    titleText:setFillColor( 14/255,28/255,54/255 )
    titleText.x = display.contentCenterX
    titleText.y = display.contentCenterY - 250

    local options =
        {
            text = "Your Height",
            width = display.contentWidth-100,
            font = defaultFont,
            fontSize = standardFontSize,
            align = "left"  -- Alignment parameter
        }

    heightText = display.newText( options )
    heightText.text = "Your Height ("..tostring(userInfo.height) .. " cm)"
    heightText:setFillColor( unpack(defaultColorScheme) )
    heightText.x = display.contentCenterX
    heightText.y = display.contentCenterY - 200

    heightField = native.newTextField( display.contentCenterX, heightText.y + 35, standardBoxWidth, 30 )
    heightField.inputType = "number"
    heightField.hasBackground = false
    heightField:addEventListener( "userInput", textListener )

    heightLine = display.newRect( heightField.x, heightField.y + 30, standardBoxWidth, 3 )
    heightLine:setFillColor( unpack(defaultColorScheme) )

    weightText = display.newText( options )
    weightText.text = "Your Weight ("..tostring(userInfo.weight).." kg)"
    weightText:setFillColor( unpack(defaultColorScheme) )
    weightText.x = heightText.x
    weightText.y = heightText.y + 100

    weightField = native.newTextField( display.contentCenterX, weightText.y + 35, standardBoxWidth, 30 )
    weightField.inputType = "number"
    weightField.hasBackground = false
    weightField:addEventListener( "userInput", textListener )

    weightLine = display.newRect( display.contentCenterX, weightField.y + 30, standardBoxWidth, 3 )
    weightLine:setFillColor( unpack(defaultColorScheme) )

    pushUpText = display.newText( options )
    pushUpText.text = "Push Up Count (" ..tostring(userInfo.pushCounter) .. " times)"
    pushUpText:setFillColor( unpack(defaultColorScheme) )
    pushUpText.x = weightLine.x
    pushUpText.y = weightLine.y + 50

    pushUpField = native.newTextField( display.contentCenterX, pushUpText.y + 35, standardBoxWidth, 30 )
    pushUpField.inputType = "number"
    pushUpField.hasBackground = false
    pushUpField:addEventListener( "userInput", textListener )

    pushUpLine = display.newRect( display.contentCenterX, pushUpField.y + 30, standardBoxWidth, 3 )
    pushUpLine:setFillColor( unpack(defaultColorScheme) )

    runText = display.newText( options )
    runText.text = "Running (" .. tostring(math.floor(userInfo.runCounter)).." minutes, " .. tostring(userInfo.runCounter%1*100).." seconds)"
    runText:setFillColor( unpack(defaultColorScheme) )
    runText.x = display.contentCenterX
    runText.y = pushUpLine.y + 50

    runField = native.newTextField( runText.x-85, runText.y + 35, 100, 26 )
    runField.inputType = "number"
    runField.hasBackground = false
    runField:addEventListener( "userInput", textListener )

    runField2 = native.newTextField( display.contentCenterX+70, runText.y + 35, 100, 26 )
    runField2.inputType = "number"
    runField2.hasBackground = false
    runField2:addEventListener( "userInput", textListener )

    runLine = display.newRect( runField.x, runField.y + 30, 100, 3 )
    runLine:setFillColor( unpack(defaultColorScheme) )

    runLine2 = display.newRect( runField2.x, runField2.y + 30, 100, 3 )
    runLine2:setFillColor( unpack(defaultColorScheme) )

    confirmBtn = widget.newButton(
        {
            left = 0,
            top = 0,
            id = "1",
            label = "Submit",
            onEvent = backScene
        })
    confirmBtn.x = display.contentCenterX
    confirmBtn.y = runLine2.y + 50

    Runtime:addEventListener( "key", onKeyEvent )

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then



    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        menubg:removeSelf()
        menubg = nil

        cardBG:removeSelf()
        cardBG = nil

        titleText:removeSelf()
        titleText = nil

        confirmBtn:removeSelf()
        confirmBtn = nil

        heightText:removeSelf()
        heightText = nil

        heightLine:removeSelf()
        heightLine = nil

        heightField:removeSelf()
        heightField = nil

        weightText:removeSelf()
        weightText = nil

        weightLine:removeSelf()
        weightLine = nil

        weightField:removeSelf()
        weightField = nil

        pushUpText:removeSelf()
        pushUpText = nil

        pushUpLine:removeSelf()
        pushUpLine = nil

        pushUpField:removeSelf()
        pushUpField = nil

        runText:removeSelf()
        runText = nil

        runField:removeSelf()
        runField = nil

        runField2:removeSelf()
        runField2 = nil

        runLine:removeSelf()
        runLine = nil

        runLine2:removeSelf()
        runLine2 = nil

        composer.setVariable("overlay",false)
        Runtime:removeEventListener( "key", onKeyEvent )

        event.parent:overlayBack()

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
