local composer = require( "composer" )
local widget = require("widget")
local loadsave = require("_Scripts.loadsave")
local scene = composer.newScene()

local sceneName, sceneIcon, labelName

local function onRowTouch( event )
    local phase = event.phase

    if phase == "press" or phase == "tap" then
        print( "Touched row:", event.target.index )
        local options = {
            isModal = true,
            effect = "fade",
            time = 400,
        }
        -- By some method such as a "pause" button, show the overlay
        composer.setVariable("overlay",true)
        composer.showOverlay( sceneName[event.target.index], options )
    end
end

local function onRowRender( event )

    --Set up the localized variables to be passed via the event table
    local row = event.row
    local id = row.index
    local params = event.row.params
    --sceneName = {"_screens.settings","_screens.stepsOverlay"}
    --labelName = {"Settings","Track Running"}
    --sceneIcon = {"assets/ui/location.png","assets/ui/location.png"}
    --row.bg = display.newRect( 0, 0, display.contentWidth, 60 )
    --row.bg.anchorX = 0
    --row.bg.anchorY = 0
    --row.bg:setFillColor( 1, 1, 1 )
    --row:insert( row.bg )

    row.icon = display.newImageRect( sceneIcon[row.id], 15,15)
    row.icon.anchorX = 0
    row.icon.anchorY = 0.5
    row.icon.x = 20
    row.icon.y = 20
    row:insert(row.icon)

    local options =
        {
            text = labelName[row.id],
            width = display.contentWidth-65,
            font = native.systemFont,
            fontSize = 18,
            align = "left"  -- Alignment parameter
        }

    row.nameText = display.newText( options )

    --row.nameText = display.newText( params.name, row.width, 0,128, native.systemFontBold, 18,"" )
    row.nameText.anchorX = 0
    row.nameText.anchorY = 0.5
    row.nameText:setFillColor( 0 )
    row.nameText.y = 20
    row.nameText.x = 50

    row:insert( row.nameText )
    --row:insert( row.phoneText )

    return true
end

function scene:create( event )

    local sceneGroup = self.view

    local defaultBG = composer.getVariable("universalBG")
    local statusBarHeight = composer.getVariable("statusHeight")
    local centerY = display.contentCenterY
    local centerX = display.contentCenterX
    local fullHeight = display.contentHeight
    local fullWidth = display.contentWidth


    local menubg = display.newImageRect( defaultBG, display.contentWidth,display.contentHeight)
    menubg.x = display.contentCenterX
    menubg.y = display.contentCenterY + statusBarHeight

    local myTblView = widget.newTableView(
    {
        left = display.contentCenterX-130,
        top = display.contentCenterY-110,
        height = fullHeight-90,
        width = fullWidth,
        friction = 0.1,
        --noLines = true,
        backgroundColor = { 1,1,1,0.8 },
        onRowRender = onRowRender,
        onRowTouch = onRowTouch,
        isBounceEnabled = false,
        listener = scrollListener
    })

    myTblView.x = centerX
    myTblView.y = centerY + 50

    sceneName = {"_screens.settings","_screens.stepsOverlay"}
    labelName = {"Settings","Track Running"}
    sceneIcon = {"assets/ui/setting.png","assets/ui/running.png"}
    local rowColor = { default={1,1,1,0.5}, over={1,0.5,0,0.2} }
    for i = 1, #labelName do
    -- Insert a row into the tableView
    myTblView:insertRow{rowHeight = 40,isCategory = false,rowColor = rowColor}
    end


    sceneGroup:insert(menubg)
    sceneGroup:insert(myTblView)

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end

function scene:destroy( event )

    local sceneGroup = self.view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
