-- screens.mainMenu

local composer = require ("composer")       -- Include the Composer library. Please refer to -> http://docs.coronalabs.com/api/library/composer/index.html
local scene = composer.newScene()           -- Created a new scene

local widget = require ("widget")     -- Included the Widget library for buttons, tabs, sliders and many more
local extendWidget = require("_Scripts.newPanel")
local extendNav = require("_Scripts.newNav")
local loadsave = require("_Scripts.loadsave")
local sqlite3 = require( "sqlite3" )

local menuContextIcon = {"assets/icons/home.png", "assets/ui/workout.png","assets/ui/nutrition.png","assets/ui/location.png","assets/ui/folder.png"}
local menuContext = {"Dashboard", "Workout","Nutrition","Tracker","Others"}
local menuScenes = {"_screens.mainMenu", "_screens.workout","_screens.diet","_screens.gymFinder","_screens.others"}
local defaultFont
local state, navBar, dropDownMenu, panel, invisPanel
local state

local function handlePages(event)
    if event.phase == "began" then
        composer.gotoScene(menuScenes[event.target.id])
    end
end

local function excuteStartScene()
    --composer.gotoScene("_screens.mainMenu", "crossFade", 100)
    --composer.gotoScene("_screens.mainMenu")
    -- Open "data.db". If the file doesn't exist, it will be created
    local path = system.pathForFile( "data.db", system.DocumentsDirectory )
    local db = sqlite3.open( path )

    -- Set up the table if it doesn't exist
    local tablesetup = [[CREATE TABLE IF NOT EXISTS FoodDB (id INTEGER PRIMARY KEY, purpose, foodName);]]
    print( tablesetup )
    db:exec( tablesetup )

    local tablesetup = [[CREATE TABLE IF NOT EXISTS ExerciseDB (id INTEGER PRIMARY KEY, duration, exercise);]]
    print( tablesetup )
    db:exec( tablesetup )

    local count = 0
    for row in db:nrows("SELECT * FROM FoodDB") do
        count = count + 1
        break
    end

    if count == 0 then
        local weightLoss = {"Eggs","Leafy Greens", "Salmon", "Chicken Breast","Green Tea","Soups","Apple Cider Vinegar", "Chia Seeds"}
        local muscleGain = {"Beef","Brown Rice", "Oranges", "Eggs", "Spinach", "Greek Yoghurt", "Whey Protein"}
        local enduranceGain = {"Chia Seeds", "Oatmeal", "Spinach", "Salmon", "Eggs","Banana"}
        local maintainWeight = {"Lemons", "Almonds", "Broccoli", "Oats", "Avocado"}

        local fullHolder={weightLoss,muscleGain,enduranceGain,maintainWeight} --weight loss, muscle gain, endurance gain, maintain weight
        local colName = {"WL","MG","EG","MW"}
        local tableFill

        for x = 1, #fullHolder do
            for i = 1, #fullHolder[x] do
                tablefill = [[INSERT INTO FoodDB VALUES (NULL, ']]..colName[x]..[[',']]..fullHolder[x][i]..[['); ]]
                db:exec( tablefill )
            end
        end

        local exercises01 = {"Jumping Jacks","Wall Sit","Push Ups","Abs Crunches","Step-Ups","Squats","Tricep Dip","Plank","High Knee","Lunges","Push Up Rotation","Side Planks"}
        local exercises02 = {"Hang Ups", "Squats", "Push Ups", "Sit Ups", "Burpees", "Plank"}
        local exercises03 = {"Body Builder", "Planks", "Push Ups", "Squats", "Pull Ups", "Flutter Kick"}
        local holder = {exercises01,exercises02,exercises03}
        local duration = {"30","40","45"}

        for x = 1, #holder do
            for i = 1, #holder[x] do
                tablefill = [[INSERT INTO ExerciseDB VALUES (NULL, ']]..duration[x]..[[',']]..holder[x][i]..[['); ]]
                db:exec( tablefill )
            end
        end
    end

    db:close()
    native.setActivityIndicator( false )
    composer.gotoScene("_screens.mainMenu")
    --composer.gotoScene("_screens.tracker")
end

function scene:create( event )
    local sceneGroup = self.view

    native.setActivityIndicator( false )

    defaultFont = composer.getVariable("universalFont")
    local centerX = display.contentCenterX
    local centerY = display.contentCenterY
    local statusbarHeight = composer.getVariable("statusHeight")

    state = false

    local buttonGrp = {}
    local imageGrp = {}
    local padding = display.contentWidth/4.6 + 5
    for i = 1, 5 do
    buttonGrp[i] = widget.newButton(
    {
        left = 0,
        top = 0,
        width = padding,
        height = 80,
        labelYOffset = 16,
        font = defaultFont,
        fontSize = 12,
        id = i,
        label = menuContext[i],
        onEvent = handlePages
    })

    buttonGrp[i].x = -38 + (i*74)
    buttonGrp[i].y = 35 + statusbarHeight
    buttonGrp[i]:setFillColor(1,1,1,0.4)

    imageGrp[i] = display.newImageRect(menuContextIcon[i],18,18)
    imageGrp[i].x = buttonGrp[i].x
    imageGrp[i].y = buttonGrp[i].y - 15

    end
    composer.setVariable("navBar",buttonGrp[1].height)
end

function scene:show( event )
    local phase = event.phase

    if ( phase == "will" ) then         -- Scene is not shown entirely

    elseif ( phase == "did" ) then      -- Scene is fully shown on the screen

        excuteStartScene()

    end
end


function scene:hide( event )
    local phase = event.phase

    if ( phase == "will" ) then         -- Scene is not off the screen entirely

    elseif ( phase == "did" ) then      -- Scene is off the screen



    end
end

function scene:destroy( event )
-- Called before the scene is removed
end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene
