local widget = require( "widget" )

function widget.newNavigationBar( options )
  local customOptions = options or {}
  local opt = {}
  opt.left = customOptions.left or nil
  opt.top = customOptions.top or nil
  opt.width = customOptions.width or display.contentWidth
  opt.height = customOptions.height or 50
  if ( customOptions.includeStatusBar == nil ) then
    opt.includeStatusBar = true -- assume status bars for business apps
  else
    opt.includeStatusBar = customOptions.includeStatusBar
  end

  -- Determine the amount of space to adjust for the presense of a status bar
  local statusBarPad = 0
  if ( opt.includeStatusBar ) then
    statusBarPad = display.topStatusBarContentHeight
  end

  opt.x = customOptions.x or display.contentCenterX
  opt.y = customOptions.y or (opt.height + statusBarPad) * 0.5
  opt.id = customOptions.id
  opt.isTransluscent = customOptions.isTransluscent or true
  opt.background = customOptions.background
  opt.backgroundColor = customOptions.backgroundColor
  opt.title = customOptions.title or ""
  opt.titleColor = customOptions.titleColor or { 0, 0, 0 }
  opt.font = customOptions.font or native.systemFontBold
  opt.fontSize = customOptions.fontSize or 18
  opt.leftButton = customOptions.leftButton or nil
  opt.rightButton = customOptions.rightButton or nil

  -- If "left" and "top" parameters are passed, calculate the X and Y
  if ( opt.left ) then
    opt.x = opt.left + opt.width * 0.5
  end
  if ( opt.top ) then
    opt.y = opt.top + (opt.height + statusBarPad) * 0.5
  end
end
